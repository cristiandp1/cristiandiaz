<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Detalle;
use App\Producto;
use App\Venta;
use Illuminate\Http\Request;
use Log;
use Validator;

class VentasController extends Controller
{
    /**
     *Lista los clientes que pueden comprar y productos que puedes ser vendidos
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function getVentas(){
        $productos = Producto::where('cantidad','>=',1)->get();
        $clientes = Cliente::where('estado',true)->get();
        return view('ventas.ventas',compact('productos','clientes'));

    }

    /**
     * Muesta el detalle de la compra
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getDetalle(Request $request){
        $rules = [
            'nombre_cliente' => 'required',
            'nombre_producto' => 'required',
            'cantidad' => 'min:1'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all)->withErrors($validator);
        }
        $detalle = collect([
            'mensaje' => null,
            'nombre_cliente' => null,
            'nombre_producto' => null,
            'cantidad' => null,
            'total' => null,
            'descuento' => 10,
            'subtotal' => null
        ]);
        $cliente = Cliente::where('nombre',$request->nombre_cliente)->where('estado',true)->first();
        if(!is_null($cliente)){
            $detalle->put('nombre_cliente',$cliente->nombre);
            $producto = Producto::where('nombre',$request->nombre_producto)->where('cantidad','!=',0)->first();
            if(!is_null($producto)){
                $detalle->put('nombre_producto',$producto->nombre);
                if($request->cantidad <= $producto->cantidad){
                    $detalle->put('cantidad',$request->cantidad);
                    $total = $producto->precio * $request->cantidad;
                    $detalle->put('subtotal', $total);
                    $descuento = ($producto->precio * $request->cantidad)*10/100;
                    $subtotal = ($total - $descuento)*19/100;
                    $detalle->put('total',($total-$descuento) + $subtotal);
                }else{
                    $detalle->put('mensaje','Producto con stock maximo de '.$producto->cantidad.' unidades');
                }
            }
        }
        return view('ventas.compras',compact('detalle'));
    }

    /**
     * Realiza la venta
     * @param Request $request
     */
    public function postComprar(Request $request){
        $resultado = false;
        $producto = Producto::where('nombre',$request->nombre_producto)->first();
        $cliente = Cliente::where('nombre',$request->nombre_cliente)->first();
        if(!is_null($producto)){
            if($request->cantidad <= $producto->cantidad){
                if(!is_null($cliente)){
                    $producto->cantidad = $producto->cantidad - $request->cantidad;
                    $producto->save();

                    $venta = new Venta();
                    $venta->iva = 19;
                    $venta->descuento = 10;
                    $venta->total = $request->total;
                    $venta->id_cliente = $cliente->id;
                    $venta->save();

                    $detalle = new Detalle();
                    $detalle->venta_id = $venta->id;
                    $detalle->cantidad = $request->cantidad;
                    $detalle->producto_id = $producto->id;
                    $detalle->subtotal = $request->subtotal;
                    $detalle->save();

                    $resultado = true;
                }
            }
        }
        if($resultado){
            $mensaje = 'Gracias por su compra';
        }else{
            $mensaje = 'Hubo un error al realizar su compra, intentelo mas tarde';
        }
        return view('ventas.confirmacion',compact('mensaje'));

    }

    public function getHistorial(){

        $detalles = Detalle::join('ventas','ventas.id','=','detalle.venta_id')
            ->join('productos','productos.id','=','ventas.id')
            ->join('clientes','clientes.id','=','ventas.id_cliente')
            ->select('clientes.nombre as nomCliente','productos.nombre as nomProducto','detalle.cantidad','detalle.subtotal','ventas.total','ventas.iva','ventas.descuento')
            ->get();
        return view('ventas.historial',compact('detalles'));
    }
}
