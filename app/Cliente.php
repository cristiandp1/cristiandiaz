<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Cliente extends Authenticatable
{
    protected $table = "clientes";
    protected $fillable = ['id', 'nombre','estado'];
    protected $dates = ['created_at','updated_at'];
}
