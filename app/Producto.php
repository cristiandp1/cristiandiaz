<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = "productos";
    protected $fillable = ['id', 'nombre','precio','cantidad'];
    protected $dates = ['created_at','updated_at'];

}
