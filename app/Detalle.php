<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalle extends Model
{
    protected $table = "detalle";
    protected $fillable = ['id', 'venta_id','cantidad','producto_id','subtotal'];
    protected $dates = ['created_at','updated_at'];
}
