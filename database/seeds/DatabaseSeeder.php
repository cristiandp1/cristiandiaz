<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cache::flush();
        $this->call(ProductosTableSeeder::class);
        $this->call(ClientesTableSeeder::class);
    }
}
