<?php

use Illuminate\Database\Seeder;
use App\Cliente;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clientes = [
            [
                'nombre' => 'Alexis Sanchéz',
                'estado' => true,
            ],
            [
                'nombre' => 'Claudio Bravo',
                'estado' => true,
            ],
            [
                'nombre' => 'Arturo Vidal',
                'estado' => false,
            ],
        ];
        foreach ($clientes as $cliente) {
            $client = Cliente::where('nombre', '=', $cliente['nombre'])->first();
            if (is_null($client)) {
                $client = new Cliente();
                $client->nombre =$cliente['nombre'];
                $client->estado = $cliente['estado'];
                $client->save();
            }
        }
    }
}
