<?php

use Illuminate\Database\Seeder;
use App\Producto;
use Illuminate\Support\Facades\Log;
class ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productos = [
            [
                'nombre' => 'Auto Colección',
                'cantidad' => 3,
                'precio' => 5000
            ],
            [
                'nombre' => 'Moto Colección',
                'cantidad' => 5,
                'precio' => 3000
            ],
            [
                'nombre' => 'Bus Colección',
                'cantidad' => 2,
                'precio' => 8000
            ],
            [
                'nombre' => 'Camión Colección',
                'cantidad' => 0,
                'precio' => 10000
            ],
        ];
        foreach ($productos as $producto) {
            $product = Producto::where('nombre', '=', $producto['nombre'])->first();
            if (is_null($product)) {
                $product = new Producto();
                $product->nombre =$producto['nombre'];
                $product->cantidad = $producto['cantidad'];
                $product->precio = $producto['precio'];
                $product->save();
            }
        }
    }
}
