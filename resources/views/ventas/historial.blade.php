<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Confirmacion</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md">
            Historial de compras
        </div>
        <div class="container flex-center">
            <table class="table table-striped table-bordered display responsive no-wrap iniciar-tabla" width="100%">
                <thead>
                <tr>
                    <th> Nombre cliente</th>
                    <th> Nombre producto </th>
                    <th> Cantidad</th>
                    <th> IVA</th>
                    <th> Descuento</th>
                    <th> Subtotal</th>
                    <th> Total</th>
                </tr>
                </thead>
                <tbody>
                @foreach($detalles as $detalle)
                    <tr>
                        <td>{{$detalle->nomCliente}}</td>
                        <td>{{$detalle->nomProducto}}</td>
                        <td>{{$detalle->cantidad}}</td>
                        <td>{{$detalle->iva}}</td>
                        <td>{{$detalle->descuento}}</td>
                        <td>{{$detalle->total}}</td>
                        <td>{{$detalle->subtotal}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <hr>
        <div class="links">
            <a href="{{route('welcome')}}">Volver</a>
        </div>
    </div>

</div>
</body>
</html>
