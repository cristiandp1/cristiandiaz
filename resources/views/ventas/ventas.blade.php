<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Productos</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>

<div class="position-ref full-height">
    <div class="flex-center position-ref full-height">
        {{--            @if (Route::has('login'))--}}
        <div class="top-right links">
                <a href="{{route('ventas.historial')}}">Historial de compras</a>
        </div>
    <div class="content">
        <div class="title m-b-md">
            Productos Disponibles
        </div>
        <div class="container flex-center">
        <form action="{{action('VentasController@getDetalle')}}" method="POST">
            {{csrf_field()}}
                <div class="form-group">
                    <label class="mr-sm-2 sr-only" for="nombre_cliente">Seleccione un Cliente</label>
                    <select class="form-control custom-select mr-sm-2" name="nombre_cliente">
                        @foreach($clientes as $cliente)
                            <option value="{{$cliente->nombre}}"> {{$cliente->nombre}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('nombre_cliente'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre_cliente') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="mr-sm-2 sr-only" for="nombre_producto">Seleccione un producto</label>
                    <select class="form-control custom-select mr-sm-2" name="nombre_producto">
                        @foreach($productos as $producto)
                            @if($producto->cantidad >=1)
                            <option value="{{$producto->nombre}}"> {{$producto->nombre}}</option>
                            @endif
                        @endforeach
                    </select>
                    @if ($errors->has('nombre_producto'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre_producto') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="cantidad">Cantidad</label>
                    <input type="text" class="form-control" name="cantidad" >
                    @if ($errors->has('cantidad'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cantidad') }}</strong>
                        </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary mb-2">Comprar</button>
            </form>
        </div>
        <hr>
        <div class="links">
            <a href="{{route('welcome')}}">Volver</a>
        </div>
    </div>
</div>
</body>
</html>
