<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Compras</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>

<div class="position-ref full-height">
@php
@endphp
    <div class="content">
        @if(is_null($detalle->get('mensaje')))
        <div class="title m-b-md">
            Confirmar Compra
        </div>
        <div class="container flex-center">
            <form action="{{action('VentasController@postComprar')}}" method="POST">
                {{csrf_field()}}
                <div class="form-group">
                    <label class="mr-sm-2 sr-only" for="nombre_cliente"> Nombre Cliente </label>
                    <input type="text" class="form-control" name="nombre_cliente" readonly="readonly" value="{{$detalle->get('nombre_cliente')}}">
                </div>
                <div class="form-group">
                    <label class="mr-sm-2 sr-only" for="nombre_producto">Nombre producto</label>
                    <input type="text" class="form-control" name="nombre_producto" readonly="readonly" value="{{$detalle->get('nombre_producto')}}">

                </div>
                <div class="form-group">
                    <label for="cantidad"> Cantidad </label>
                    <input type="text" class="form-control" name="cantidad" readonly="readonly" value="{{$detalle->get('cantidad')}}">
                </div>
                <div class="form-group">
                    <label for="cantidad"> Subtotal </label>
                    <input type="text" class="form-control" name="total" readonly="readonly" value="{{$detalle->get('subtotal')}}">
                </div>
                <div class="form-group">
                    <label for="cantidad"> Descuento </label>
                    <input type="text" class="form-control" name="descuento" readonly="readonly" value="{{$detalle->get('descuento')}}">
                </div>
                <div class="form-group">
                    <label for="cantidad"> Total </label>
                    <input type="text" class="form-control" name="subtotal" readonly="readonly" value="{{$detalle->get('total')}}">
                </div>
                <hr>
                <button type="submit" class="btn btn-primary mb-2">Confirmar Compra</button>
            </form>
        </div>
        @else
            <div class="flex-center position-ref full-height">
                <div class="content">
                    <div class="title m-b-md">
                        {{$detalle->get('mensaje')}}
                    </div>
                </div>
            </div>
        @endif
        <hr>
        <div class="links">
            <a href="{{route('ventas.ventas')}}">Volver</a>
        </div>
    </div>
</div>
</body>
</html>
