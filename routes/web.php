<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

/* Ventas */
Route::get('/ventas', [
    'uses' => 'VentasController@getVentas',
    'as' => 'ventas.ventas'
]);

Route::post('/ventasdetalle', [
    'uses' => 'VentasController@getDetalle',
    'as' => 'ventas.detalle'
]);

Route::post('/ventascompras', [
    'uses' => 'VentasController@postComprar',
    'as' => 'ventas.compras'
]);

Route::get('/historial', [
    'uses' => 'VentasController@getHistorial',
    'as' => 'ventas.historial'
]);
