-- Query 1
SELECT empresa.nombre, persona.nombre, vehiculo.descripcion, vehiculo.tipo FROM wherex_db.vehiculo 
join persona on vehiculo.persona_id = persona.id
join empresa on persona.empresa_id = empresa.id
where tipo = 1;

-- Query 2
SELECT persona.id, persona.nombre, persona.estado FROM wherex_db.persona 
join empresa on persona.empresa_id = empresa.id
where empresa.id = 3 and persona.estado = 1;

-- Query 3
Select persona.nombre, empresa.nombre, vehiculo.descripcion from wherex_db.vehiculo
right join persona on vehiculo.persona_id = persona.id
join empresa on persona.empresa_id = empresa.id
order by empresa_id asc, persona.nombre asc;