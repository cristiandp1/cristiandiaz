/*** Instalación del proyecto***/

Proyecto realizado en Laravel 5.5 y MySQL


1.- ejecutar comandos....

	composer install
	
	php artisan key:generate
	
2.- configurar su .env o copiar el example ademas crear la base

3.- Ejecutar comandos...
	
	php artisan migrate
	
	php artisan db:seed
------------------------------------------------------------------

Al realizar el proyecto tome algunas consideraciones... ejemplo:

1.- El iva seria 19% y el descuento para todos los productos seria del 10%.

2.- Debido al tiempo se crearon seeder para que se poblara la base de clientes y productos.

3.- se le agrego el campo id_cliente a la tabla de ventas para asi poder realacionar un cliente con una venta.

4.- por el momento solo se pueden realizar compras y ver el historial de ellas.

-------------------------------------------------------------------

Las preguntas 2 y 3 se podran descargar a traves del sitio o revisar dentro del proyecto la direccion:
	public/archivos/query.sql 			-> para la pregunta 2
	public/archivos/Pregunta 3.docx 	-> para la pregunta 3